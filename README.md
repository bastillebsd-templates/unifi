# Unifi
Bastille template to bootstrap Unifi

## Status
[![pipeline status](https://gitlab.com/bastillebsd-templates/unifi/badges/master/pipeline.svg)](https://gitlab.com/bastillebsd-templates/unifi/commits/master)

## Bootstrap
```shell
bastille bootstrap https://gitlab.com/bastillebsd-templates/unifi
```

## Usage
```shell
bastille template TARGET bastillebsd-templates/unifi
```
